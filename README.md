# Music OCR #

Android app for the recognition and playback of the notes of a music sheet starting from a photograph.

Project created for the Computer Vision course (2016-2017) of Politecnico di Torino.

Co-author: Antonio Greco