package com.cv.polito.omr;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.AsyncTask;

import java.util.ArrayList;

public class SoundHandlerTask extends AsyncTask<ArrayList<MusicNote>, Integer, Boolean> {

    private final int sampleRate = 8000;
    private final int beatPerMinute = 100;
    public AudioTrack audioTrack;

    public SoundHandlerTask() {
    }

    @Override
    protected Boolean doInBackground(ArrayList<MusicNote>... params) {
        ArrayList<MusicNote> notes = params[0];
        int totalNumSamples = 0;
        for(MusicNote note : notes) {
            totalNumSamples += note.duration;
        }
        totalNumSamples *= sampleRate;

        audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO,
                AudioFormat.ENCODING_PCM_16BIT, totalNumSamples,
                AudioTrack.MODE_STATIC);

        // set setNotificationMarkerPosition accounting audio length
        audioTrack.setNotificationMarkerPosition(totalNumSamples/2);

        // now add OnPlaybackPositionUpdateListener to audioTrack
        audioTrack.setPlaybackPositionUpdateListener(
                new AudioTrack.OnPlaybackPositionUpdateListener() {
                    @Override
                    public void onMarkerReached(AudioTrack track) {
                        ResultActivity.soundTaskCompleted = true;
                    }

                    @Override
                    public void onPeriodicNotification(AudioTrack track) {

                    }
                });

        byte generatedSnd[] = new byte[2 * totalNumSamples];

        int idx = 0;
        for(MusicNote note : notes) {
            Float duration = note.duration / (beatPerMinute / 60);
            int numSamples = (int) (duration * sampleRate);
            Double samples[] = new Double[numSamples];

            Double freqOfTone = 0.0;
            switch(note.pitch) {
                case DO1:
                    freqOfTone = 261.6;
                    break;
                case RE1:
                    freqOfTone = 293.7;
                    break;
                case MI1:
                    freqOfTone = 329.6;
                    break;
                case FA1:
                    freqOfTone = 349.2;
                    break;
                case SOL1:
                    freqOfTone = 392.0;
                    break;
                case LA1:
                    freqOfTone = 440.0;
                    break;
                case SI1:
                    freqOfTone = 493.9;
                    break;
                case DO2:
                    freqOfTone = 523.3;
                    break;
                case RE2:
                    freqOfTone = 587.4;
                    break;
                case MI2:
                    freqOfTone = 659.2;
                    break;
                case FA2:
                    freqOfTone = 698.4;
                    break;
                case SOL2:
                    freqOfTone = 784.0;
                    break;
                case LA2:
                    freqOfTone = 880.0;
                    break;
                case PAUSE:
                    freqOfTone = 0.0;
                    break;

            }

            if(freqOfTone != 0.0) {
                for (int i = 0; i < numSamples; ++i) {
                    samples[i] = Math.sin(2 * Math.PI * i / (sampleRate/freqOfTone));
                }
            }
            else {
                for (int i = 0; i < numSamples; ++i) {
                    samples[i] = 0.0;
                }
            }

            // convert to 16 bit pcm sound array
            // assumes the sample buffer is normalised.

            for (double dVal : samples) {
                short val = (short) (dVal * 32767);
                generatedSnd[idx++] = (byte) (val & 0x00ff);
                generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
            }
            idx -= numSamples;
        }
        audioTrack.write(generatedSnd, 0, totalNumSamples);
        audioTrack.play();
        return true;
    }
}
