package com.cv.polito.omr;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    public ImageView imageView;
    private ArrayList<MusicNote> notes;
    public static Boolean soundTaskCompleted;
    private SoundHandlerTask soundTask;
    private String photoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        soundTaskCompleted = true;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        imageView = (ImageView) this.findViewById(R.id.imageView3);
        soundTask = new SoundHandlerTask();

        Intent intent = getIntent();
        photoPath = intent.getStringExtra("BitmapImagePath");

        NoteArrayWrapper n = (NoteArrayWrapper) intent.getSerializableExtra("NotesList");
        notes = n.getNotes();

        Button playButton = (Button) this.findViewById(R.id.button);

        if(notes.size() == 0){
            playButton.setEnabled(false);
            TextView textView = (TextView) this.findViewById(R.id.textView2);
            textView.setText("No notes detected.");
        }

        playButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (soundTaskCompleted == true) {
                    soundTaskCompleted = false;
                    soundTask.execute(notes);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        ((BitmapDrawable) imageView.getDrawable()).getBitmap().recycle();

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        if(soundTask.audioTrack != null) {
            soundTask.audioTrack.stop();
            soundTask.audioTrack.release();
        }
        startActivity(intent);
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            setPic(photoPath);
        }
    }

    private void setPic(String photoPath) {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.max(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, bmOptions);
        imageView.setImageBitmap(bitmap);
        imageView.setRotation(getCameraPhotoOrientation(photoPath));
    }

    public int getCameraPhotoOrientation(String imagePath) {
        int rotate = 0;
        try {
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

}
