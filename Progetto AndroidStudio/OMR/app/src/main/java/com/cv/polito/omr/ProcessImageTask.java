package com.cv.polito.omr;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static com.cv.polito.omr.ProcessingActivity.photo;

public class ProcessImageTask extends AsyncTask<Bitmap, Integer, ArrayList<MusicNote>> {

    static{ System.loadLibrary("opencv_java3"); }

    private static final String TAG = "OMR_ProcessImageTask";

    private final Context context;

    public enum Pitch {
        PAUSE, DO1, RE1, MI1, FA1, SOL1, LA1, SI1, DO2, RE2, MI2, FA2, SOL2, LA2
    }

    private class MusicElement {
        RotatedRect ellipse;
        Pitch pitch;
        float duration;

        public MusicElement(RotatedRect ellipse, Pitch pitch, float duration) {
            this.ellipse = ellipse;
            this.pitch = pitch;
            this.duration = duration;
        }
    }

    public ProcessImageTask(Context context){
        this.context=context;
    }



    @Override
    protected ArrayList<MusicNote> doInBackground(Bitmap... params) {

        publishProgress(new Integer(0));

        Mat sourceImage = new Mat();
        //Convert bitmap to Mat
        Utils.bitmapToMat(params[0], sourceImage);
        Imgproc.cvtColor(sourceImage, sourceImage, Imgproc.COLOR_BGR2GRAY);

        //Rotate the image if required (information found in orientation tag)
        try {
            rotateImageIfRequired(sourceImage,ProcessingActivity.photoPath);
        }
        catch(IOException e) {
            Log.e(TAG, "Error during photo rotation.");
        }

        //Initial analysis to get an approximation of staff height
        double medianStaffHeight = initialAnalysis(sourceImage);
        if(medianStaffHeight == -1) {
            return new ArrayList<MusicNote>();
        }

        publishProgress(new Integer(5));

        //Initial blur to delete some noise
        Imgproc.blur(sourceImage, sourceImage, new Size(3,3));

        //Thresholding 
        int threshValue = (int) (0.35*medianStaffHeight+7.91);
        if(threshValue%2 == 0) {
            threshValue += 1;
        }
        Imgproc.adaptiveThreshold(sourceImage, sourceImage, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C , Imgproc.THRESH_BINARY_INV, threshValue, 7);

        //Analysis to find a list of staves
        Mat processedImage = new Mat();
        sourceImage.copyTo(processedImage);

        //Process the image in order to make it easier to detect entire staves
        staffLinesBlending(processedImage, medianStaffHeight);

        //Find the contours of staves
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(processedImage, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE );

        publishProgress(new Integer(15));

        //Create a list of Mats, containing only the staves
        List<Mat> staves = new ArrayList<>();
        for(MatOfPoint c : contours) {
            //Process only contours with a specific height
            if(c.height() > medianStaffHeight*0.2 && c.height() < medianStaffHeight*5.0 ) {

                //Remove everything except the possible staff from this portion of image, creating a mask based on its contour c
                Mat mask = Mat.zeros(sourceImage.size(), CvType.CV_8U);
                List<MatOfPoint> cList = new ArrayList<>();
                cList.add(c);
                Imgproc.drawContours(mask, cList, -1, new Scalar(255,255,255), Core.FILLED);
                Imgproc.dilate(mask, mask, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(medianStaffHeight*0.2,medianStaffHeight*0.2))); // Dilate mask in order to better include all notes in the stave
                Mat sourceCopy = new Mat();
                sourceImage.copyTo(sourceCopy, mask);

                //Rotation correction: Find bounding box of current contour and rotate the image based on angle of BB
                MatOfPoint2f m2f = new MatOfPoint2f( c.toArray() );
                RotatedRect box = Imgproc.minAreaRect(m2f);
                double angle = box.angle;
                if(box.size.height > box.size.width) {
                    angle += 90;
                }
                Mat rotationMatrix = Imgproc.getRotationMatrix2D(box.center, angle, 1);
                Imgproc.warpAffine(sourceCopy, sourceCopy, rotationMatrix, sourceCopy.size(), Imgproc.INTER_CUBIC );

                //Crop the image to reduce its dimension (reducing complexity of following operation)
                Rect initialCropping = box.boundingRect();
                initialCropping.x -= 0.5*initialCropping.width;
                initialCropping.y -= 0.5*initialCropping.height;
                initialCropping.height += 1.0*initialCropping.height;
                initialCropping.width += 1.0*initialCropping.width;

                if(initialCropping.y < 0) {
                    initialCropping.y = 0;
                }
                if(initialCropping.x < 0) {
                    initialCropping.x = 0;
                }
                if((initialCropping.y + initialCropping.height) > sourceCopy.rows()) {
                    initialCropping.height = sourceCopy.rows() - initialCropping.y;
                }
                if((initialCropping.x + initialCropping.width) > sourceCopy.cols()) {
                    initialCropping.width = sourceCopy.cols() - initialCropping.x;
                }
                sourceCopy = sourceCopy.submat(initialCropping);

                //Perspective correction
                List<Point> pointsDestination = perspectiveCorrection(sourceCopy, medianStaffHeight);
                if (pointsDestination == null) {
                    continue;
                }

                //Final staff cropping
                Rect croppingRect = new Rect(pointsDestination.get(0), pointsDestination.get(2));
                //Expand the rectangle
                croppingRect.y -= 0.75*croppingRect.height;
                if(croppingRect.y < 0) {
                    croppingRect.y = 0;
                }
                croppingRect.height += 1.5*croppingRect.height;
                if((croppingRect.y + croppingRect.height) > sourceCopy.rows()) {
                    croppingRect.height = sourceCopy.rows() - croppingRect.y;
                }
                if((croppingRect.x + croppingRect.width) > sourceCopy.cols()) {
                    croppingRect.width = sourceCopy.cols() - croppingRect.x;
                }

                //Add the final image to staves list
                Mat temp = new Mat();
                temp = sourceCopy.submat(croppingRect);
                staves.add(temp);

            }
        }
        Collections.reverse(staves);

        publishProgress(new Integer(30));

        //Loop over the list "staves", detecting the notes and adding them to a list of MusicElements
        List<MusicElement> sheet = new ArrayList<>();

        //Debug
        int j =-1;

        //For each image in staves
        for(Mat m : staves) {

            //Image Analysis in order to detect exactly five staff lines and their position

            //Extract lines from image
            Mat lines = new Mat();
            m.copyTo(lines);
            // Create structure element for extracting horizontal lines through morphological operations
            int horizontalsize = lines.cols() / 50;  // Specify size on horizontal axis
            if(horizontalsize == 0)
                horizontalsize = 1;
            Mat horizontalStructure = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(horizontalsize,1));
            // Apply morphological operations (opening operation)
            Imgproc.erode(lines, lines, horizontalStructure, new Point(-1, -1), 1);
            Imgproc.dilate(lines, lines, horizontalStructure, new Point(-1, -1), 1);

            //Calculate histogram
            Mat histogram = Mat.zeros(lines.rows(), 1, CvType.CV_32S);
            for(int i=0;i<lines.rows();i++)
            {
                histogram.put(i, 0, Core.countNonZero(lines.row(i)));
            }

            //Peak detection in histogram

            //staffLines contains a Point for each staff line, saving in its x the vertical position and in its y the peak value
            Point[] staffLines = new Point[]{new Point(0,0), new Point(0,0), new Point(0,0), new Point(0,0), new Point(0,0)};
            Double prev=0.0, curr=histogram.get(0,0)[0], next=histogram.get(1,0)[0];
            Double threshold = 0.3*lines.cols(); //Consider only peaks higher than this threshold
            int pos = 0;
            int average = 0;
            for(int i = 0; i < histogram.rows(); i++) {

                if(curr > threshold) {
                    if(curr >= prev && curr >= next && pos <= 4) {
                        //peak detected
                        if(curr > staffLines[pos].y) {
                            staffLines[pos].x = i;
                            staffLines[pos].y = curr;
                        }
                        else if(curr == staffLines[pos].y) {
                            //Handling of flat peaks, in other words handling of line thickness > 1
                            average++;
                        }
                    }
                    if(next <= threshold) {
                        if(pos <= 4) {
                            staffLines[pos].x += (int)(average/2);
                            average = 0;
                        }

                        pos++;
                    }
                }

                prev = curr;
                curr = next;
                if(i == (histogram.rows()-1)) {
                    next = 0.0;
                }
                else {
                    next = histogram.get((i+1),0)[0];
                }
            }

            if(pos != 5) { //if pos (number of peaks detected) != 5, there is no staff detected, jump to the next image
                continue;
            }

            //Calculate average distance from a line to the next one
            double avgDistance = 0.0;
            for(int i = 0; i < staffLines.length-1; i++) {
                avgDistance += staffLines[i+1].x - staffLines[i].x;
            }
            avgDistance /= 4.0;

            //Debug
            j++;
            String staffLinesMessage = "staffLines" + j;
            for(Point p : staffLines) {
                staffLinesMessage += " " + p.toString();
            }
            Log.d(TAG,staffLinesMessage);
            Log.d(TAG,"avgDistance" + j + " " + avgDistance);


            //Create linesPosition, which contains all lines and interlines positions
            double[] linesPosition = new double[13];
            int k = 0; //Index of array staffLines
            for(int i = 0; i < linesPosition.length; i++) {
                if(i%2 == 0) {
                    if(i == 0) {
                        linesPosition[i] = staffLines[0].x - avgDistance;
                    }
                    else if(i == (linesPosition.length-1)) {
                        linesPosition[i] = staffLines[4].x + avgDistance;
                    }
                    else {
                        linesPosition[i] = staffLines[k].x;
                        k++;
                    }
                }
                else {
                    if(i == 1) {
                        linesPosition[i] = staffLines[0].x - (avgDistance/2);
                    }
                    else if(i == (linesPosition.length-2)) {
                        linesPosition[i] = staffLines[4].x + (avgDistance/2);
                    }
                    else {
                        linesPosition[i] = (staffLines[k-1].x + staffLines[k].x) / 2;
                    }
                }
            }


            //Note detection in current staff:

            Mat notes = new Mat();
            m.copyTo(notes);

            //Remove staff lines
            removeStaffLines(notes, avgDistance, linesPosition);

            //Save current image into Mat mNoLines for later use
            Mat mNoLines = new Mat();
            notes.copyTo(mNoLines);
            Imgproc.threshold(mNoLines, mNoLines, 150, 255, Imgproc.THRESH_BINARY);

            //Fill holes in 2/4 - 4/4 notes (closing operation)
            Imgproc.dilate(notes, notes, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(avgDistance*0.7, 1)));
            Imgproc.erode(notes, notes, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(avgDistance*0.7, 1)));

            //Delete vertical bars
            int barSize = (int) (avgDistance*0.45);
            Mat barStructure = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(barSize,1));
            //Apply morphological operations (opening operation)
            Imgproc.erode(notes, notes, barStructure, new Point(-1, -1), 1);
            Imgproc.dilate(notes, notes, barStructure, new Point(-1, -1), 1);
            Imgproc.blur(notes, notes, new Size(3,3));
            Imgproc.threshold(notes, notes, 100, 255, Imgproc.THRESH_BINARY);

            //Closing operation to close eventual holes in 1/4 pauses
            Imgproc.dilate(notes, notes, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1,avgDistance*0.7)));
            Imgproc.erode(notes, notes, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1,avgDistance*0.7)));

            // Remove initial key and last vertical bar
            Rect noKey = new Rect(new Point(3.4*avgDistance, 0), new Point(notes.cols() - (avgDistance*2), notes.rows()));
            notes = notes.submat(noKey);

            //Find ellipses in the image
            List<RotatedRect> ellipsesList = findEllipses(notes, avgDistance);

            //Scan the list, discerning notes from pauses / key / tempo with ellipses height
            for(RotatedRect r : ellipsesList) {
                Rect rect = r.boundingRect();
                // Ignore ellipses higher than 3.5 * avgDistance (e.g. key, tempo), shorter than 0.3*avgDistance and larger than 0.6*avgDistance (errors, noises)
                if(rect.height < (3.5*avgDistance) && rect.height > (0.3*avgDistance) && rect.width > (0.6*avgDistance)) {

                    if(rect.height < 0.6*avgDistance && r.center.y < linesPosition[6] && r.center.y > linesPosition[4]) {
                        //This is a 2/4 or 4/4 pause
                        if(Math.abs(linesPosition[4] - r.center.y) < Math.abs(r.center.y - linesPosition[6])) {
                            //If the ellipse is closer to the second line, the pause duration is 4/4
                            sheet.add(new MusicElement(r, Pitch.PAUSE, 4));
                        }
                        else {
                            //If the ellipse is closer to the third line, the pause duration is 2/4
                            sheet.add(new MusicElement(r, Pitch.PAUSE, 2));
                        }
                    }
                    else if(rect.height > 2.0*avgDistance && r.center.y > linesPosition[5] && r.center.y < linesPosition[8] && rect.width < 1.5*avgDistance) {
                        //This is a 1/4 pause
                        sheet.add(new MusicElement(r, Pitch.PAUSE, 1));
                    }
                    else {
                        //This is a note

                        //Calculate distance from every line or interline, choose the smallest one to detect the pitch (index of linesPosition saved in minIndex)
                        double min = Double.MAX_VALUE;
                        int minIndex = -1;
                        for(int i = 0; i < linesPosition.length; i++) {
                            double distance = Math.abs(r.center.y - linesPosition[i]);
                            if(distance < min) {
                                min = distance;
                                minIndex = i;
                            }
                        }

                        //Calculate duration of the note
                        float duration = calculateNoteDuration(m.submat(noKey), mNoLines.submat(noKey), minIndex, r, rect, avgDistance);

                        switch(minIndex) {
                            case 0:
                                sheet.add(new MusicElement(r,Pitch.LA2,duration));
                                break;
                            case 1:
                                sheet.add(new MusicElement(r,Pitch.SOL2,duration));
                                break;
                            case 2:
                                sheet.add(new MusicElement(r,Pitch.FA2,duration));
                                break;
                            case 3:
                                sheet.add(new MusicElement(r,Pitch.MI2,duration));
                                break;
                            case 4:
                                sheet.add(new MusicElement(r,Pitch.RE2,duration));
                                break;
                            case 5:
                                sheet.add(new MusicElement(r,Pitch.DO2,duration));
                                break;
                            case 6:
                                sheet.add(new MusicElement(r,Pitch.SI1,duration));
                                break;
                            case 7:
                                sheet.add(new MusicElement(r,Pitch.LA1,duration));
                                break;
                            case 8:
                                sheet.add(new MusicElement(r,Pitch.SOL1,duration));
                                break;
                            case 9:
                                sheet.add(new MusicElement(r,Pitch.FA1,duration));
                                break;
                            case 10:
                                sheet.add(new MusicElement(r,Pitch.MI1,duration));
                                break;
                            case 11:
                                sheet.add(new MusicElement(r,Pitch.RE1,duration));
                                break;
                            case 12:
                                sheet.add(new MusicElement(r,Pitch.DO1,duration));
                                break;
                            case -1:
                                break;

                        }

                    }

                }
            }

        }

        publishProgress(new Integer(100));

        //Debug
        for(MusicElement me : sheet) {
            Log.d(TAG, me.pitch + " " + me.duration);
        }

        ArrayList<MusicNote> result = new ArrayList<>();
        for(MusicElement me : sheet) {
            MusicNote m = new MusicNote(me.pitch, me.duration);
            result.add(m);
        }

        return result;
    }

    private void rotateImageIfRequired(Mat photo, String photoPath) throws IOException {
        ExifInterface ei = new ExifInterface(photoPath);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                Core.transpose(photo, photo);
                Core.flip(photo, photo, 1);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                Core.flip(photo, photo, -1);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                Core.transpose(photo, photo);
                Core.flip(photo, photo, 0);
                break;

            case ExifInterface.ORIENTATION_NORMAL:

            default:
                break;
        }

    }

    private double initialAnalysis(Mat sourceImage) {
        Mat analysisImage = new Mat();
        sourceImage.copyTo(analysisImage);
        Imgproc.blur(analysisImage, analysisImage, new Size(3,3));
        Imgproc.adaptiveThreshold(analysisImage, analysisImage, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV, 55, 7);
        //Closing
        Imgproc.dilate(analysisImage, analysisImage, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(20,20)));
        Imgproc.erode(analysisImage, analysisImage, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(20,20)));

        Mat analysisResult = new Mat();
        analysisImage.copyTo(analysisResult);
        Imgproc.cvtColor(analysisResult, analysisResult, Imgproc.COLOR_GRAY2BGR);
        List<MatOfPoint> analysisContours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(analysisImage, analysisContours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE ); // Find the contours in the image
        List<Double> staffHeights = new ArrayList<>();
        for(MatOfPoint c : analysisContours) {
            if(Imgproc.contourArea(c) > 0.005*sourceImage.width()*sourceImage.height() ) {
                MatOfPoint2f m2f = new MatOfPoint2f( c.toArray() );
                RotatedRect box = Imgproc.minAreaRect(m2f);
                Point[] p = new Point[4];
                box.points(p);
                MatOfPoint m = new MatOfPoint();
                m.fromArray(p);
                List<MatOfPoint> l1 = new ArrayList<>();
                l1.add(m);
                Imgproc.polylines(analysisResult, l1, true, new Scalar(255,0,0), 3);
                List<MatOfPoint> l2 = new ArrayList<>();
                l2.add(c);
                Imgproc.drawContours(analysisResult, l2, -1, new Scalar(0,255,0), 3);

                if(box.size.height < box.size.width) {
                    staffHeights.add(box.size.height);
                }
                else {
                    staffHeights.add(box.size.width);
                }
            }
        }
        if(staffHeights.size() == 0) {
            Log.e(TAG, "No staves detected.");
            return -1;
        }
        Collections.sort(staffHeights);

        //Debug
        Log.d(TAG, "staffHeights " + staffHeights );
        Log.d(TAG, "median StaffHeight " + staffHeights.get(staffHeights.size()/2));
        return staffHeights.get(staffHeights.size()/2);
    }

    private void staffLinesBlending(Mat processedImage, double medianStaffHeight) {
        //Blur+Threshold to remove noise after threshold operation
        Imgproc.blur(processedImage,processedImage, new Size(7,7));
        Imgproc.threshold(processedImage, processedImage, 128, 255, Imgproc.THRESH_BINARY);

        //Closing (close space between staff lines)
        Imgproc.dilate(processedImage, processedImage, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(medianStaffHeight*0.2, medianStaffHeight*0.2)));
        Imgproc.erode(processedImage, processedImage, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(medianStaffHeight*0.2, medianStaffHeight*0.2)));

        //Opening to remove imperfections
        Imgproc.erode(processedImage, processedImage, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(medianStaffHeight*0.2, medianStaffHeight*0.2)));
        Imgproc.dilate(processedImage, processedImage, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(medianStaffHeight*0.2, medianStaffHeight*0.2)));
    }

    private List<Point> perspectiveCorrection(Mat sourceCopy, double medianStaffHeight) {
        //Recalculate contour for image resulting from rotation correction (sourceCopy)
        Mat processedCopy = new Mat();
        sourceCopy.copyTo(processedCopy);
        //Replicate previous transformations (opening, closing), blending staff lines
        Imgproc.dilate(processedCopy, processedCopy, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(medianStaffHeight*0.2, medianStaffHeight*0.2)));
        Imgproc.erode(processedCopy, processedCopy, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(medianStaffHeight*0.2, medianStaffHeight*0.2)));
        Imgproc.erode(processedCopy, processedCopy, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(medianStaffHeight*0.5, medianStaffHeight*0.5)));
        Imgproc.dilate(processedCopy, processedCopy, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(medianStaffHeight*0.5, medianStaffHeight*0.5)));
        List<MatOfPoint> tempContours = new ArrayList<>();
        Imgproc.findContours(processedCopy, tempContours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE ); // Find the contours in the image
        if(tempContours.size() == 0) {
            return null;
        }

        //Calculate convex hull from contour (tempContours.get(0) is the only contour we find)
        MatOfInt hull= new MatOfInt();
        Imgproc.convexHull(tempContours.get(0), hull);

        //Transform convex hull MatOfInt into Point[]
        Point[] points = new Point[hull.rows()];
        for(int j=0; j < hull.rows(); j++){
            int index = (int)hull.get(j, 0)[0];
            points[j] = new Point(tempContours.get(0).get(index, 0)[0], tempContours.get(0).get(index, 0)[1]);
        }
        //Transform convex hull Point[] into MatOfPoint
        MatOfPoint mop = new MatOfPoint();
        mop.fromArray(points);
        //Transform convex hull MatOfPoint into MatOfPoint2f
        MatOfPoint2f m2fHull = new MatOfPoint2f( mop.toArray() );

        //Apply approximation on convex hull
        double epsilon = 5;
        MatOfPoint2f approx = new MatOfPoint2f();
        Imgproc.approxPolyDP(m2fHull, approx, epsilon, true);
        Point[] points2 = approx.toArray(); //Array containing points of simplified convex hull

        //Find starting points and destination points for perspective correction
        List<Point> pointsSource = new ArrayList<>(); //It will be a rectangle made of the two points to the most left and the two points to the most right of the simplified convex hull
        List<Point> pointsDestination = new ArrayList<>();
        double max1=Double.MIN_VALUE, max2=Double.MIN_VALUE, max1Index=-1, max2Index=-1, min1=Double.MAX_VALUE, min2=Double.MAX_VALUE, min1Index=-1, min2Index=-1;
        int index = 0;
        for(Point p : points2) {
            if(p.x < min2) {
                if(p.x < min1) {
                    min2 = min1;
                    min2Index = min1Index;

                    min1 = p.x;
                    min1Index = index;
                }
                else {
                    min2 = p.x;
                    min2Index = index;
                }
            }
            if(p.x > max2) {
                if(p.x > max1) {
                    max2 = max1;
                    max2Index = max1Index;

                    max1 = p.x;
                    max1Index = index;
                }
                else {
                    max2 = p.x;
                    max2Index = index;
                }
            }
            index++;
        }

        if(points2[(int)min1Index].y < points2[(int)min2Index].y) {
            pointsSource.add(points2[(int)min1Index]);
            pointsSource.add(points2[(int)min2Index]);
        }
        else {
            pointsSource.add(points2[(int)min2Index]);
            pointsSource.add(points2[(int)min1Index]);
        }
        if(points2[(int)max1Index].y > points2[(int)max2Index].y) {
            pointsSource.add(points2[(int)max1Index]);
            pointsSource.add(points2[(int)max2Index]);
        }
        else {
            pointsSource.add(points2[(int)max2Index]);
            pointsSource.add(points2[(int)max1Index]);
        }

        double xmin = Double.MAX_VALUE, xmax = Double.MIN_VALUE, ymin = Double.MAX_VALUE, ymax = Double.MIN_VALUE;
        for(Point p : pointsSource) {
            if(p.x < xmin) {
                xmin = p.x;
            }
            if(p.x > xmax) {
                xmax = p.x;
            }
            if(p.y < ymin) {
                ymin = p.y;
            }
            if(p.y > ymax) {
                ymax = p.y;
            }
        }

        pointsDestination.add(new Point(xmin,ymin));
        pointsDestination.add(new Point(xmin,ymax));
        pointsDestination.add(new Point(xmax,ymax));
        pointsDestination.add(new Point(xmax,ymin));

        //Apply perspective correction
        Mat sourceM = Converters.vector_Point2f_to_Mat(pointsSource);
        Mat destinationM = Converters.vector_Point2f_to_Mat(pointsDestination);
        Mat perspectiveTransform = Imgproc.getPerspectiveTransform(sourceM, destinationM);
        Imgproc.warpPerspective(sourceCopy, sourceCopy, perspectiveTransform, sourceCopy.size(), Imgproc.INTER_CUBIC);

        return pointsDestination;
    }

    private void removeStaffLines(Mat notes, double avgDistance, double[] linesPosition) {

        //Staff lines removal based on linesPosition
        for(int i = 2; i <= 10; i=i+2) {
            int index = (int) linesPosition[i];
            for(int x = 0; x < notes.cols(); x++) {
                double avgColor = 0;
                int tempIndex = 0;
                for(int y = (int) (-avgDistance*0.4); y <= (int) (avgDistance*0.4); y++) {
                    if(index + y > 0 && index + y < notes.rows()) {
                        avgColor += notes.get(index+y, x)[0];
                        tempIndex++;
                    }
                }
                avgColor /= tempIndex;
                if(avgColor < 150) {
                    for(int y = (int) (-avgDistance*0.2); y <= (int) (avgDistance*0.2); y++) {
                        if(index + y > 0 && index + y < notes.rows()) {
                            notes.put(index+y, x, 0);
                        }
                    }
                }
            }
        }

        //Remove eventual remains of staff lines
        // Create structure element to remove horizontal lines through morphological operations
        int verticalSize = (int) (avgDistance*0.3);
        Mat verticalStructure = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1, verticalSize));
        // Apply morphological operations (opening operation)
        Imgproc.erode(notes, notes, verticalStructure, new Point(-1, -1), 1);
        Imgproc.dilate(notes, notes, verticalStructure, new Point(-1, -1), 1);

    }

    private List<RotatedRect> findEllipses(Mat notes, double avgDistance) {

        //Find contours in the image
        List<MatOfPoint> contoursNotes = new ArrayList<>();
        Mat notesCopy = new Mat();
        notes.copyTo(notesCopy);
        Imgproc.findContours(notesCopy, contoursNotes, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE);

        //Find ellipses from contours, adding them to a list
        List<RotatedRect> ellipsesList = new LinkedList<>();
        for(MatOfPoint c : contoursNotes) {
            MatOfPoint2f m2f = new MatOfPoint2f( c.toArray() );
            if(m2f.rows() < 5){
                continue;
            }
            RotatedRect r = Imgproc.fitEllipse(m2f);
            ellipsesList.add(r);
        }
        //Sort the ellipses by their increasing x of center point
        for(int i = 1; i < ellipsesList.size(); i++) {
            RotatedRect value = ellipsesList.get(i);
            int h = i-1;
            while( h>=0 && value.center.x < ellipsesList.get(h).center.x) {
                ellipsesList.set(h+1, ellipsesList.get(h));
                h--;
            }
            ellipsesList.set(h+1, value);
        }

        //When 2 (or more) rectangles are too close to each other, consider only the biggest one
        for(int i = 0; i < ellipsesList.size(); i++) {
            //Compare with previous element
            if(i != 0 ) {
                if(ellipsesList.get(i).center.x - ellipsesList.get(i-1).center.x < 1.0*avgDistance) {
                    if(ellipsesList.get(i).size.width*ellipsesList.get(i).size.height > ellipsesList.get(i-1).size.width*ellipsesList.get(i-1).size.height) {
                        ellipsesList.remove(i-1);
                    }
                    else {
                        ellipsesList.remove(i);
                    }
                    i--;
                }
            }
        }

        return ellipsesList;
    }

    private float calculateNoteDuration(Mat m, Mat mNoLines, int minIndex, RotatedRect r, Rect rect, double avgDistance) {
        Mat noteROIMat = new Mat();
        m.copyTo(noteROIMat);

        //Define region of interest to detect filled notes or hollow notes
        Rect noteROI = new Rect();
        if(minIndex%2 == 0) {
            //Note is on a line
            if(minIndex <= 12 && minIndex >= 7 ) {
                // Note between DO1 and LA1
                noteROI = new Rect(new Point(r.center.x - (rect.width/5) - 1, r.center.y), new Size(3,(rect.height/4)));
            }
            else {
                // Note between SI1 and LA2
                noteROI = new Rect(new Point(r.center.x + (rect.width/5) - 1, r.center.y - (rect.height/4)), new Size(3,(rect.height/4)));
            }
            Mat noteROIMat2 = new Mat();
            m.copyTo(noteROIMat2);
            noteROIMat2 = noteROIMat2.submat(noteROI);
            Imgproc.threshold(noteROIMat2, noteROIMat2, 230, 255, Imgproc.THRESH_BINARY);

            //If a hollow note is not detected, give a second chance with a different ROI
            if(Core.countNonZero(noteROIMat2) == (noteROIMat2.height()*noteROIMat2.width())) {
                noteROI = new Rect(new Point(r.center.x, r.center.y - (rect.height/5)), new Size(1,(rect.height/6)));
            }
        }
        else {
            //Note is in a space
            noteROI = new Rect(new Point(r.center.x, r.center.y - (rect.height/8)), new Size(1,(rect.height/4)));
        }

        if(noteROI.x < 0) {
            noteROI.x = 0;
        }
        if(noteROI.y < 0) {
            noteROI.y = 0;
        }
        if(noteROI.x + noteROI.width > noteROIMat.cols()) {
            noteROI.x = noteROIMat.cols() - noteROI.width;
        }
        if(noteROI.y + noteROI.height > noteROIMat.rows()) {
            noteROI.y = noteROIMat.rows() - noteROI.height;
        }
        noteROIMat = noteROIMat.submat(noteROI);
        Imgproc.threshold(noteROIMat, noteROIMat, 230, 255, Imgproc.THRESH_BINARY);

        float duration = 0.0f;
        if(Core.countNonZero(noteROIMat) < (noteROIMat.height()*noteROIMat.width())) {
            //There is at least one black pixel
            //The note duration is 2/4 or 4/4

            //Define region of interest to detect note bars
            Rect barROI;
            Mat barROIMat = new Mat();
            mNoLines.copyTo(barROIMat);

            if(minIndex <= 12 && minIndex >= 7 ) {
                //if the note pitch is between DO1 and LA1
                barROI = new Rect((int) (rect.x + rect.width/2), (int) (rect.y - avgDistance), rect.width/2, 2);
            }
            else {
                //if the note pitch is between SI1 and LA2
                barROI = new Rect((int) (rect.x), (int) (rect.y + rect.height), rect.width/2, 2);
            }

            if(barROI.x < 0) {
                barROI.x = 0;
            }
            if(barROI.y < 0) {
                barROI.y = 0;
            }
            if(barROI.width < 0) {
                barROI.width = 0;
            }
            if(barROI.height < 0) {
                barROI.height = 0;
            }
            if(barROI.x + barROI.width > barROIMat.cols()) {
                if(barROI.width > barROIMat.cols()) {
                    barROI.width = barROIMat.cols() - barROI.x;
                }
                else {
                    barROI.x = barROIMat.cols() - barROI.width;
                }
            }
            if(barROI.y + barROI.height > barROIMat.rows()) {
                if(barROI.height > barROIMat.rows()) {
                    barROI.height = barROIMat.rows() - barROI.y;
                }
                else {
                    barROI.y = barROIMat.rows() - barROI.height;
                }
            }
            barROIMat = barROIMat.submat(barROI);

            if(Core.countNonZero(barROIMat) > (0.01 * barROIMat.height()*barROIMat.width()) ) {
                //if there is at least one white pixel, there is a bar -> note duration is 2/4
                duration = 2.0f;
            }
            else {
                //with no white pixels, note duration is 4/4
                duration = 4.0f;
            }

        }
        else {
            //There are no black pixels
            //The note duration is 1/4
            duration = 1.0f;
        }

        return duration;
    }




    protected void onPostExecute(ArrayList<MusicNote> notes) {
        //Deallocate bitmap photo
        if(photo!=null)
        {
            photo.recycle();
            photo=null;
        }
        ((BitmapDrawable) ProcessingActivity.imageView.getDrawable()).getBitmap().recycle();

        Intent intent = new Intent(context, ResultActivity.class);
        intent.putExtra("BitmapImagePath", ProcessingActivity.photoPath);

        NoteArrayWrapper n = new NoteArrayWrapper(notes);
        intent.putExtra("NotesList", n);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        context.startActivity(intent);
    }

    protected void onProgressUpdate(Integer... progress) {
        ProcessingActivity.text.setText("Processing Image... " + progress[0] + "%");
    }

}
