package com.cv.polito.omr;

import java.io.Serializable;
import java.util.ArrayList;

public class NoteArrayWrapper implements Serializable{

    private ArrayList<MusicNote> notes;

    public NoteArrayWrapper(ArrayList<MusicNote> notes) {
        this.notes = notes;
    }

    public ArrayList<MusicNote> getNotes() {
        return notes;
    }

}
