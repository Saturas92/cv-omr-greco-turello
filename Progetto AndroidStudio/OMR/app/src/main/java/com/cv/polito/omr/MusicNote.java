package com.cv.polito.omr;

import java.io.Serializable;

public class MusicNote implements Serializable {

    public ProcessImageTask.Pitch pitch;
    public float duration;

    public MusicNote(ProcessImageTask.Pitch pitch, float duration) {
        this.pitch = pitch;
        this.duration = duration;
    }
}
